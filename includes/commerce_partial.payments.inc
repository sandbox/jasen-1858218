<?php

/**
 * Partial Payments
 */
function commerce_partial_payments($uid, $order_id) {
  global $user;
  $order = commerce_order_load($order_id);
  if ($user->uid != $order->uid) {
    return drupal_access_denied();
  }

  drupal_set_title('Payments');

  $output = commerce_embed_view('commerce_payment_order', 'order_payments_simple', array($order_id));
  return $output;
}



